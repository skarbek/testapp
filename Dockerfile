FROM golang:1.19.4 as builder

RUN apt update && \
    apt-get install -y upx

COPY main.go .
COPY go.mod .

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -a -installsuffix cgo -o testapp .
RUN upx -9 -o testapp.up testapp

FROM scratch

COPY --from=builder /go/testapp.up /

CMD ["/testapp.up"]
