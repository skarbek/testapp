package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Port desire port to run the webservice
var Port = 5000

// Version indicates the version of this application
var Version = "0.0.1"

// ShutdownDelay - time in seconds to delay shutting down the application
var ShutdownDelay = 30

// State indicates if we've received a SIGTERM/SIGKILL
var State = ""

// Response is a basic JSON response for the `ready` endpoint
type Response struct {
	State   string
	Version string
	PID     int
}

func ready(w http.ResponseWriter, r *http.Request) {
	pid := os.Getpid()
	response := Response{State, Version, pid}
	responseJSON, err := json.Marshal(response)

	if err != nil {
		panic(err)
	}

	fmt.Fprintf(w, string(responseJSON))
	fmt.Printf("GET: %s\n", r.URL.String())
}

func hello(w http.ResponseWriter, r *http.Request) {
	hostname, _ := os.Hostname()
	fmt.Fprintf(w, "Hostname: %s\nMethod: %s\nURL: %s\nHeaders: %v\nHost: %s\n", hostname, r.Method, r.URL.String(), r.Header, r.Host)
	fmt.Printf("GET: %s\n", r.URL.String())
}

func main() {
	fmt.Printf("Listening on port %d\n", Port)
	cancelChan := make(chan os.Signal, 1)
	signal.Notify(cancelChan, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		http.HandleFunc("/", hello)
		http.HandleFunc("/-/readiness", ready)
		http.ListenAndServe(fmt.Sprintf(":%d", Port), nil)
	}()
	sig := <-cancelChan
	State = fmt.Sprintf("%s", sig)
	fmt.Printf("Caught signal %v, shuttdown down in %v seconds...", sig, ShutdownDelay)
	if sig == syscall.SIGTERM || sig == syscall.SIGINT {
		time.Sleep(time.Duration(ShutdownDelay) * time.Second)
	}
}
